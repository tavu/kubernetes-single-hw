coordinator = {
  coordinator-1 = {
    name  = "coordinator-1"
    mem   = "4096"
    cpu   = "4"
    storage = {
      data_docker = {
        size = 10737418240*3 # 30gb in bytes
      }
    }
  }
  coordinator-2 = {
    name  = "coordinator-2"
    mem   = "4096"
    cpu   = "4"
    storage = {
      data_docker = {
        size = 10737418240*3
      }
    }
  }
  coordinator-3 = {
    name  = "coordinator-3"
    mem   = "4096"
    cpu   = "4"
    storage = {
      data_docker = {
        size = 10737418240*3
      }
    }
  }
}

worker = {
  worker-1 = {
    name  = "worker-1"
    mem   = "4096"
    cpu   = "4"
    storage = {
      data_docker = {
        size = 10737418240*3
      }
      data_ceph_mon = {
        size = 10737418240*3
      }
      data_ceph_osd = {
        size = 10737418240*3
      }
    }
  }
  worker-2 = {
    name  = "worker-2"
    mem   = "4096"
    cpu   = "4"
    storage = {
      data_docker = {
        size = 10737418240*3
      }
      data_ceph_mon = {
        size = 10737418240*3
      }
      data_ceph_osd = {
        size = 10737418240*3
      }
    }
  }
  worker-3 = {
    name  = "worker-3"
    mem   = "4096"
    cpu   = "4"
    storage = {
      data_docker = {
        size = 10737418240*3
      }
      data_ceph_mon = {
        size = 10737418240*3
      }
      data_ceph_osd = {
        size = 10737418240*3
      }
    }
  }
}
