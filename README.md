
(Highly experimental)

# Kubernetes Cluster on Single Bare-metal Hardware

## Aims

To provide:

* Production like environment for development use

* Able to implement stuff in cloud native way and achieve things like:
  - High deployment frequency: on-demand, multiple deploys per day
  - lead time for changes: less than hour
  - time to restore service: less than hour

* If in hurry and out of budget, you can deploy cluster to world-wide-web also
  - Lots on caveats on this, but if this acceptable then why not

## Notes

### libvirt

* On Ubuntu distros SELinux is enforced by qemu even if it is disabled globally, this might cause unexpected `Could not open '/var/lib/libvirt/images/<FILE_NAME>': Permission denied` errors. Double check that `security_driver = "none"` is uncommented in `/etc/libvirt/qemu.conf` and issue `sudo systemctl restart libvirt-bin` to restart the daemon.

