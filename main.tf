terraform {
  backend "gcs" {
    bucket = "kubernetes_terraform_states"
    prefix = "clusters/phase1"
  }
  required_providers {
    libvirt = {
      source = "dmacvicar/libvirt"
      version = "0.6.11"
    }
  }
}

# See docs: https://registry.terraform.io/providers/dmacvicar/libvirt/latest/docs
provider "libvirt" {
    uri = "qemu:///system"
}

