module "libvirt_kubernetes_coordinator" {
  source   = "./modules/libvirt-node"
  for_each = var.coordinator

  name             = each.value.name
  cpu              = each.value.cpu
  mem              = each.value.mem
  storage          = each.value.storage
}

module "libvirt_kubernetes_worker" {
  source   = "./modules/libvirt-node"
  for_each = var.worker

  name             = each.value.name
  cpu              = each.value.cpu
  mem              = each.value.mem
  storage          = each.value.storage
}

