.PHONY: all test clean

#
# gcloud install
#
.gcloud.install:
	echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
	sudo apt-get -yy install apt-transport-https ca-certificates gnupg
	curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -
	sudo apt-get update && sudo apt-get -yy install google-cloud-sdk
	touch .gcloud.install

.gcloud.init:
	gcloud init
	touch .gcloud.init

gcloud: .gcloud.install .gcloud.init

#
# terraform install
#
.terraform.install:
	curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
	sudo apt-add-repository "deb [arch=$(shell dpkg --print-architecture)] https://apt.releases.hashicorp.com $(shell lsb_release -cs) main"
	sudo apt install terraform
	touch .terraform.install

.terraform.init:
	echo "FIXME: gcloud auth application-default login"
	echo "FIXME: gsutil mb -p rd-kubernetes-single-hw gs://kubernetes_terraform_states"
	terraform init
	terraform workspace new kubernetes-single-hw
	touch .terraform.init

terraform: .terraform.install .terraform.init


todo:
	echo "NOTHING HERE YET"

#
# MAIN PROCESS
#
phase-1: gcloud terraform

phase-2: todo

phase-3: todo
