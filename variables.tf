variable "ssh_pubkey" {
  description = "Root SSH Key"
  type        = string
  default     = "put-your-ssh-pubkey-here"
}

variable "coordinator" {
  description = "Kubernetes Coordinator Nodes"
  type = map(object({
    name    = string
    mem     = string
    cpu     = string
    storage = map(object({
      size = string
    }))
  }))
}

variable "worker" {
  description = "Kubernetes Worker Nodes"
  type = map(object({
    name    = string
    mem     = string
    cpu     = string
    storage = map(object({
      size = string
    }))
  }))
}
