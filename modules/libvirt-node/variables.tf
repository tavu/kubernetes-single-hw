variable "ssh_pubkey" {
  description = "SSH Root Key"
  type = string
  default = "put-your-ssh-pubkey-here"
}

variable "name" {
  description = "Domain Name"
  type = string
  default = "noname"
}

variable "mem" {
  description = "Domain Memory"
  type = string
  default = "2048"
}

variable "cpu" {
  description = "Domain CPU"
  type = string
  default = "2"
}

variable "storage" {
  description = "Data disk"
  type = map(object({
    size = string
  }))
}
