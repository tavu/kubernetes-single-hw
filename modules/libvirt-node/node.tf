resource "libvirt_volume" "os_image_ubuntu" {
  name  = "os_image_ubuntu"
  pool  = "default"
  source = "/var/lib/libvirt/images/ubuntu-20.04-server-cloudimg-amd64.img"
}

resource "libvirt_volume" "node_disk" {
  name           = "${var.name}_node_disk"
  base_volume_id = libvirt_volume.os_image_ubuntu.id
  pool           = "default"
  size           = 10737418240*3 # 30gb
}

resource "libvirt_volume" "data_disk" {
  for_each = var.storage
  name = "${var.name}_${each.key}"
  size = each.value.size
}

resource "libvirt_cloudinit_disk" "cloudinit_node" {
  name = "${var.name}_node_cloudinit"
  pool = "default"

  user_data = <<EOF
#cloud-config
fqdn: ${var.name}.local
disable_root: 0
ssh_pwauth: 0
users:
  - name: root
    ssh-authorized-keys:
      - ${file("authorized_keys")}
growpart:
  mode: auto
  devices: ['/']
disk_setup:
  /dev/vdb:
    table_type: mbr
    layout: true
  /dev/vdc:
    table_type: mbr
    layout: true
  #/dev/vdd:
  #  table_type: mbr
  #  layout: true
fs_setup:
  - label: docker
    filesystem: ext4
    device: /dev/vdb
  - label: ceph_mon
    filesystem: ext4
    device: /dev/vdc
  #- label: ceph_osd
  #  filesystem: ext4
  #  device: /dev/vdd
mounts:
  - [ vdb, /var/lib/docker, "auto", "defaults,nofail", "0", "0" ]
  - [ vdc, /var/lib/rook, "auto", "defaults,nofail", "0", "0" ]
EOF

}

resource "libvirt_domain" "domain_node" {
  name = var.name
  memory = var.mem
  vcpu = var.cpu
  autostart = true

  cloudinit = libvirt_cloudinit_disk.cloudinit_node.id

  network_interface {
    network_name   = "default"
    wait_for_lease = true
  }

  console {
    type = "pty"
    target_port = "0"
    target_type = "serial"
  }

  console {
    type = "pty"
    target_type = "virtio"
    target_port = "1"
  }

  graphics {
    type = "spice"
    listen_type = "address"
    autoport = true
  }
  
  disk {
    volume_id = libvirt_volume.node_disk.id
  }

  #
  # FIXME: Storage disk is attached to domain in arbitrary order
  #
  dynamic "disk" {
    for_each = var.storage
    content {
      volume_id = libvirt_volume.data_disk["${disk.key}"].id
    }
  }

}

